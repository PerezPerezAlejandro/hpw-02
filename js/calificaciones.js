function retornaPromedio(){
  var tabla_calificaciones= document.getElementById('tabla_calificaciones');
  var tabla_body= tabla_calificaciones.lastElementChild;
  var suma=0;
  for(var i=0; i<tabla_body.children.length; i++){
    suma += Number(tabla_body.children[i].children[2].textContent);
  }
  var promedio= suma/tabla_body.children.length;
  return promedio;
}

function retornaMateriaMejorCalificacion(){
  var tabla_calificaciones= document.getElementById('tabla_calificaciones');
  var tabla_body= tabla_calificaciones.lastElementChild;
  if(tabla_body.children.length>0)
    var mayor_calificacion= Number(tabla_body.children[0].children[2].textContent);
    var clave= tabla_body.children[0].children[0].textContent;
    var materia= tabla_body.children[0].children[1].textContent;
    for(var i=0; i<tabla_body.children.length; i++){
      if(Number(tabla_body.children[i].children[2].textContent) > mayor_calificacion){
        mayor_calificacion= Number(tabla_body.children[i].children[2].textContent);
        clave= tabla_body.children[i].children[0].textContent;
        materia= tabla_body.children[i].children[1].textContent;
      }
    }
    return clave + " " + materia + " " + mayor_calificacion;
}

function retornaMateriaMenorCalificacion(){
  var tabla_calificaciones= document.getElementById('tabla_calificaciones');
  var tabla_body= tabla_calificaciones.lastElementChild;
  if(tabla_body.children.length>0)
    var menor_calificacion= Number(tabla_body.children[0].children[2].textContent);
    var clave= tabla_body.children[0].children[0].textContent;
    var materia= tabla_body.children[0].children[1].textContent;
    for(var i=0; i<tabla_body.children.length; i++){
      if(Number(tabla_body.children[i].children[2].textContent) < menor_calificacion){
        menor_calificacion= Number(tabla_body.children[i].children[2].textContent);
        clave= tabla_body.children[i].children[0].textContent;
        materia= tabla_body.children[i].children[1].textContent;
      }
    }
    return clave + " " + materia + " " + menor_calificacion;
}

/* 
Mediante este archivo JS se asignan los valores para las listas al ejecutar el HTML porque inclui
   el .js en el HTML
*/

var li_promedio= document.getElementById('li_promedio');
li_promedio.textContent= "Promedio: " + retornaPromedio();

var li_mejor_calificacion= document.getElementById('li_mejor_calificacion');
li_mejor_calificacion.textContent= "Materia con mejor calificacion: " + retornaMateriaMejorCalificacion();

var li_menor_calificacion= document.getElementById('li_menor_calificacion');
li_menor_calificacion.textContent= "Materia con menor calificacion: " + retornaMateriaMenorCalificacion();

